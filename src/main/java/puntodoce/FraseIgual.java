package puntodoce;

import javax.swing.*;

public class FraseIgual {

    public void Iguales(String texto1, String texto2){

        if(texto1.length() == texto2.length()){
            if(texto1.equals(texto2)) JOptionPane.showMessageDialog(null,"Las palabras" + texto1+ " y " + texto2 + " son iguales.");
            else {
                for (int i = 0; i < texto1.length(); i++) {
                    if (texto1.charAt(i) != texto2.charAt(i)) {
                        JOptionPane.showMessageDialog(null," Las palabras no coinciden en la posición: " + i );
                        JOptionPane.showMessageDialog(null,"Donde se tiene "+ texto1.charAt(i) + " en vez de " + texto2.charAt(i));
                    }
                }
            }
        }
        else{
            JOptionPane.showMessageDialog(null,"Las palabras no son iguales.");
        }


    }
}