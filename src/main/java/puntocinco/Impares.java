package puntocinco;

import javax.swing.*;

public class Impares {
    public void calcularPares(){
        int numerox = 1;
        while (numerox <= 100) {
            int temp = numerox % 2;
            if (temp == 0) {
                JOptionPane.showMessageDialog(null,"El número " + numerox + " es par.");
            }
            numerox++;
        }
    }
    public void calcularImpares(){
        int numeroy = 1;
        while (numeroy <= 100) {
            int temp = numeroy % 2;
            if (temp != 0) {
                JOptionPane.showMessageDialog(null,"El número " + numeroy + " es impar.");
            }
            numeroy++;
        }
    }
}

